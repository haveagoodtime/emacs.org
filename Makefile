EE ?= emacs -Q --batch --eval "(require 'ob-tangle)"

all: init.org
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

softclean:
	@rm -rf init.el

clean:
	@rm -rf init.el elpa*
